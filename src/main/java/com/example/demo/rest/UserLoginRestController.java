package com.example.demo.rest;

import com.example.demo.entity.UserLogin;
import com.example.demo.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PostUpdate;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserLoginRestController {
    private UserLoginService userLoginService;

    @Autowired
    public UserLoginRestController(UserLoginService userLoginService) {
        this.userLoginService = userLoginService;
    }

    @GetMapping("/userlogins")
    public List<UserLogin> findAll() {
        return userLoginService.findAll();
    }

    @GetMapping("/userlogins/{id}")
    public UserLogin findById(@PathVariable int id) {
        UserLogin userLogin = userLoginService.findById(id);
        if(userLogin == null) {
            throw new RuntimeException("Did not find user login with id: " + id);
        }

        return userLogin;
    }

    @GetMapping("/userlogins/username/{username}")
    public UserLogin findByUserName(@PathVariable String username) {
        UserLogin userLogin = userLoginService.findByUsername(username);
        if(userLogin == null) {
            throw new RuntimeException("Did not find user login with username: " + username);
        }

        return userLogin;
    }

    @PostMapping("/userlogins")
    public UserLogin addUserLogin(@RequestBody UserLogin userLogin) {
        userLogin.setId(0);
        userLoginService.save(userLogin);
        return userLogin;
    }

    @DeleteMapping("/userlogins/{id}")
    public String addUserLogin(@PathVariable int id) {
        userLoginService.deleteById(id);

        return "DONE";
    }

    @PutMapping("/userlogins")
    public UserLogin updateUser(@RequestBody UserLogin userLogin) {
        userLoginService.save(userLogin);
        return userLogin;
    }
}
