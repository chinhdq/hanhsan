package com.example.demo.service;

import com.example.demo.dao.UserLoginRepository;
import com.example.demo.entity.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserLoginServiceImpl implements UserLoginService{
    private UserLoginRepository userLoginRepository;

    @Autowired
    public UserLoginServiceImpl(UserLoginRepository userLoginRepository) {
        this.userLoginRepository = userLoginRepository;
    }

    @Override
    public List<UserLogin> findAll() {
        return userLoginRepository.findAll();
    }

    @Override
    public UserLogin findById(int id) {
        Optional<UserLogin> optional =  userLoginRepository.findById(id);
        if(optional.isPresent()) {
            return optional.get();
        }else {
            throw new RuntimeException("Did not find the user with id: "+ id);
        }
    }

    @Override
    public UserLogin findByUsername(String username) {
        UserLogin found =  userLoginRepository.findByUsername(username);
        if(found != null) {
            return found;
        }else {
            throw new RuntimeException("Did not find the user with username: "+ username);
        }
    }

    @Override
    public void save(UserLogin userLogin) {
        userLoginRepository.save(userLogin);
    }

    @Override
    public void deleteById(int id) {
        userLoginRepository.deleteById(id);
    }

    @Override
    public void updateUserStatus(String userName, int status) {
//        userLoginRepository.updateUserStatus(userName, status);
    }
}
