package com.example.demo.service;

import com.example.demo.entity.UserLogin;

import java.util.List;

public interface UserLoginService {
    List<UserLogin> findAll();

    UserLogin findById(int id);
    UserLogin findByUsername(String username);

    void save(UserLogin userLogin);

    void deleteById(int id);

    void updateUserStatus(String userName, int status);
}
