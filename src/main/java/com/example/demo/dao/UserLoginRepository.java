package com.example.demo.dao;

import com.example.demo.entity.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserLoginRepository extends JpaRepository<UserLogin, Integer> {
    UserLogin findByUsername(String username);
}
